import CICDEnvUtils
import hudson.model.Cause
import pom

// This Model is used for non java callout Proxies
def call(String branchType, String build_number) {

    echo " Stating CI for branchType = ${branchType}"

    envUtils = new CICDEnvUtils();
    def config = envUtils.getEnviromentConfigs()
    String edgeEnv = config.scm.branch_edgeEnv_mapping[branchType]
    String apigeeMavenGlobalOptions = envUtils.getApigeeMavenOptions(edgeEnv);
    emailUtils = new EmailUtils();

    String proxyRootDirectory = ".";
    def apigeeEnvInfo;
    def javaDirectoryExists = false;

//    node {

    try {

        def node = tool name: 'apigee-nodejs', type: 'nodejs'
        env.PATH = "${node}/bin:${env.PATH}"


        def mavenHome = tool name: 'apigee-maven', type: 'maven'




        stage('build-setup') {

            String targetEnvForBranch = config.scm.branch_edgeEnv_mapping[branchType]
            assert (targetEnvForBranch != null)
            apigeeEnvInfo = config.apigee.edge.envs[targetEnvForBranch]
            assert (apigeeEnvInfo != null)

            echo "config.com.cicd.apigee =  ${config.apigee}"
            currentBuild.displayName = "${branchType}-${build_number}"
            echo " Build Name reset ${currentBuild.displayName}"
            echo " Apigee Maven Options : ${apigeeMavenGlobalOptions}"


            if (fileExists("edge")) {
                proxyRootDirectory = "edge"
            }

            echo "proxyRootDirectory : ${proxyRootDirectory}"

            if (fileExists("java")) {
                javaDirectoryExists = true
            } else {
                echo(" checking java location " + javaDirectoryExists)
            }

            withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                              credentialsId   : config.apigee.mgmt.credential,
                              usernameVariable: 'edgeUser',
                              passwordVariable: 'edgePassword']]) {
                if (config.apigee.mgmt.OAuth) {
                    apigeeMavenOptions = apigeeMavenGlobalOptions +
                            "-Dusername=${env.edgeUser} " +
                            "-Dpassword=${env.edgePassword} " +
                            "-Dauthtype=oauth";
                } else {
                    apigeeMavenOptions = apigeeMavenGlobalOptions +
                            "-Dusername=${env.edgeUser} " +
                            "-Dpassword=${env.edgePassword} " +
                            "-Dauthtype=basic";
                }
                echo " Apigee Maven Options : ${apigeeMavenOptions}"
            }

        }

//      stage('Checkout') {
//        checkout scm
//      }

        if (javaDirectoryExists) {
            stage('build-java-callout') {
                dir('java') {
                    runCommand "mvn package"
                }
            }
        }

        stage('unit-test') {
            dir(proxyRootDirectory) {
                echo "run unit tests "
                runCommand "mvn test"
                runCommand "npm install"
                runCommand "node node_modules/istanbul/lib/cli.js cover " +
                        "--dir target/unit-test-coverage " +
                        "node_modules/mocha/bin/_mocha test/unit"

                if (true) {
                    echo "publish html report"
                    publishHTML(target: [
                            allowMissing         : false,
                            alwaysLinkToLastBuild: false,
                            keepAll              : true,
                            reportDir            : "target/unit-test-coverage/lcov-report",
                            reportFiles          : 'index.html',
                            reportName           : 'HTML Report'
                    ])
                }
            }
        }

        stage('proxy-lint') {
            dir(proxyRootDirectory) {
                script {
                    runCommand "apigeelint -s apiproxy -f html.js > target/unit-test-coverage/lcov-report/apigeelint.html"
                    if (true) {
                        echo "publish html report"
                        publishHTML(target: [
                                allowMissing         : false,
                                alwaysLinkToLastBuild: false,
                                keepAll              : true,
                                reportDir            : 'target/unit-test-coverage/lcov-report',
                                reportFiles          : 'apigeelint.html',
                                reportName           : 'Apigeelint HTML Report'
                        ])

                        //junit 'target/unit-test-coverage/**/*.xml'
                    }

                }
            }
        }

        stage('build-proxy') {
            dir(proxyRootDirectory) {
                echo "Package proxy"
                withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                  credentialsId   : config.apigee.mgmt.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {
                    if (config.apigee.mgmt.OAuth) {
                        def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                "-Dusername=${env.edgeUser} " +
                                "-Dpassword=${env.edgePassword} " +
                                "-Dauthtype=oauth";
                    } else {
                        def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                "-Dusername=${env.edgeUser} " +
                                "-Dpassword=${env.edgePassword} " +
                                "-Dauthtype=basic";
                    }

                    if (config.apigee.edge.noDeploy) {
                        echo "mvn test package " +
                                " -Ddeployment.suffix=cicd " +
                                apigeeMavenOptions

                    } else {
                        runCommand "mvn package " +
                                " -Ddeployment.suffix=cicd " +
                                apigeeMavenOptions
                    }


                }
            }
        }

        stage('pre-deploy-prep') {
            dir(proxyRootDirectory) {
                withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                  credentialsId   : config.apigee.mgmt.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {
                    if (config.apigee.mgmt.OAuth) {
                        def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                "-Dusername=${env.edgeUser} " +
                                "-Dpassword=${env.edgePassword} " +
                                "-Dauthtype=oauth";
                    } else {
                        def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                "-Dusername=${env.edgeUser} " +
                                "-Dpassword=${env.edgePassword} " +
                                "-Dauthtype=basic";
                    }

                    if (config.apigee.edge.noDeploy) {
                        echo "upload Caches "
                        echo "mvn " + apigeeMavenOptions +
                                " apigee-config:caches"
                        echo "upload Target Servers"
                        echo "mvn " + apigeeMavenOptions +
                                " apigee-config:targetservers"
                        echo "upload KVMs"
                        echo "mvn  " + apigeeMavenOptions +
                                " apigee-config:keyvaluemaps "
                    } else {
                        echo "upload Caches "
                        runCommand "mvn " + apigeeMavenOptions +
                                " apigee-config:caches"
                        echo "upload Target Servers"
                        runCommand "mvn " + apigeeMavenOptions +
                                " apigee-config:targetservers"
                        echo "upload KVMs"
                        runCommand "mvn  " + apigeeMavenOptions +
                                " apigee-config:keyvaluemaps "
                    }
                }
            }
        }
        stage('deploy-proxy') {
            if (!config.apigee.edge.noDeploy) {
                dir(proxyRootDirectory) {
                    withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                      credentialsId   : config.apigee.mgmt.credential,
                                      usernameVariable: 'edgeUser',
                                      passwordVariable: 'edgePassword']]) {

                        if (config.apigee.mgmt.OAuth) {
                            def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                    "-Dusername=${env.edgeUser} " +
                                    "-Dpassword=${env.edgePassword} " +
                                    "-Dauthtype=oauth";
                        } else {
                            def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                    "-Dusername=${env.edgeUser} " +
                                    "-Dpassword=${env.edgePassword} " +
                                    "-Dauthtype=basic";
                        }

                        echo "deploy the proxy bundle"
                        runCommand "mvn apigee-enterprise:deploy " +
                                "-Ddeployment.suffix=cicd " +
                                apigeeMavenOptions

                        apiName = getPom().artifactId("pom.xml")
                        println("adding ${apiName} to teams")
//            new TeamService().addApiToTeams(apiName)
                    }
                }
            }
        }


        stage('integration-tests') {
            if (true) {
                dir(proxyRootDirectory) {
                    withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                      credentialsId   : config.apigee.mgmt.credential,
                                      usernameVariable: 'edgeUser',
                                      passwordVariable: 'edgePassword']]) {
                        if (config.apigee.mgmt.OAuth) {
                            def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                    "-Dusername=${env.edgeUser} " +
                                    "-Dpassword=${env.edgePassword} " +
                                    "-Dauthtype=oauth";
                        } else {
                            def apigeeMavenOptions = apigeeMavenGlobalOptions +
                                    "-Dusername=${env.edgeUser} " +
                                    "-Dpassword=${env.edgePassword} " +
                                    "-Dauthtype=basic";
                        }

                        echo "load api product for integration test"
                        runCommand "mvn " + apigeeMavenOptions + " apigee-config:apiproducts"

                        echo "load api developer for integration test"
                        runCommand "mvn " + apigeeMavenOptions + " apigee-config:developers "

                        echo "load api developer app for integration test"
                        runCommand "mvn " + apigeeMavenOptions + " apigee-config:apps"

                        echo "export app key for integration test"
                        runCommand "mvn apigee-config:exportAppKeys " + apigeeMavenOptions;
                        // "-Ddeployment.suffix=cicd " ;

                        echo "run integration test"
                        runCommand "node ./node_modules/cucumber/bin/cucumber.js " +
                                "target/test/integration/features " +
                                "--format json:target/reports.json"
                    }

                    echo "genereate integration test report "
                    step([
                            $class             : 'CucumberReportPublisher',
                            fileExcludePattern : '',
                            fileIncludePattern : "**/reports.json",
                            ignoreFailedTests  : false,
                            jenkinsBasePath    : '',
                            jsonReportDirectory: "target",
                            missingFails       : false,
                            parallelTesting    : false,
                            pendingFails       : false,
                            skippedFails       : false,
                            undefinedFails     : false
                    ])
                }
            }
        }

        stage('upload-artifact') {
            if (config.artifactRepository.enabled) {
                dir(proxyRootDirectory) {
                    def pomVersion = getPom().version("pom.xml")
                    if (!pomVersion) {
                        pomVersion = getPom().version("../pom.xml")
                    }
                    // Only release a SNAPSHOT.. Skip if a SNAPSHOT is not detected
                    if ((pomVersion as String).trim().endsWith("-SNAPSHOT")) {
                        runCommand "mvn -Ddeployment.suffix=cicd -DskipTests=true deploy "
                    }
                }
            } else {
                echo 'Skip Artifact Deployment'
            }
        }

        if (config.jenkins.emailOnSuccess) {
            currentBuild.result = 'SUCCESS'
        }
    } catch (any) {
        println any.toString()
        currentBuild.result = 'FAILURE'

    }
//  }
}


def getApigeeMavenOptions(Map apigeeEnvInfo) {
    def apigeeMavenOptions = " -Papigee  " +
            "-Denv=${apigeeEnvInfo.env} " +
            "-Dorg=${apigeeEnvInfo.org} " +
            "-Dusername=${env.edgeUser} " +
            "-Dpassword=${env.edgePassword} ";
    return apigeeMavenOptions as String;
}

def getUsernameForBuild() {
    def causes = currentBuild.rawBuild.getCauses()
    for (Cause cause in causes) {
        def user;
        if (cause instanceof hudson.model.Cause.UserIdCause) {
            hudson.model.Cause.UserIdCause userIdCause = cause;
            user = userIdCause.getUserName()
            return user
        }
    }
    return null
}

def isBuildCauseUserAction() {
    def causes = currentBuild.rawBuild.getCauses()
    for (Cause cause in causes) {
        if (cause instanceof hudson.model.Cause.UserIdCause) return true
    }
    return false
}

def getPom() { return new pom(); }

def runCommand(String command) {
    if (!isUnix()) {
        println command
        if (command.trim().toLowerCase().startsWith("mvn")) {
            withMaven(globalMavenSettingsConfig: 'demo-maven-settings', maven: 'apigee-maven') {
                bat returnStdout: true, script: "${command}"
            }
        } else {

            bat returnStdout: true, script: "${command}"
        }
    } else {
        println command
        if (command.trim().toLowerCase().startsWith("mvn")) {
            withMaven(globalMavenSettingsConfig: 'demo-maven-settings', maven: 'apigee-maven') {
                sh returnStdout: true, script: command
            }
        } else {
            sh returnStdout: true, script: command
        }

    }
}

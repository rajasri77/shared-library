import CICDEnvUtils
import hudson.model.Cause
import pom

// This Model is used for non java callout Proxies
def call(String envName, String build_number) {

  echo " Stating Deploy On Production Env = ${envName}"

  envUtils = new CICDEnvUtils();
  def config = envUtils.getEnviromentConfigs()
  String apigeeMavenGlobalOptions = envUtils.getApigeeMavenOptions(envName);
  String edgeEnvName = config.apigee.edge.envs[envName].env
  emailUtils = new EmailUtils();

  String proxyRootDirectory = "edge";
  def apigeeEnvInfo;
  def javaDirectoryExists = false;

    node {

      try {

      String giturl = null
      String deployVersion = null

      stage('init') {

        deleteDir();

        if ( ! edgeEnvName ) throw new Exception("Edge Environemnt ${envName} is not supported for deployment ")

        if (!params.api) {
          error "API Name is Required "
        }

        if (!params.version) {
          error "Version is Required "
        }


        if (config.apigee.edge.envs.dev) {
          apigeeEnvInfo = config.apigee.edge.envs.dev
        }

        currentBuild.displayName = "D-${envName}-${build_number}"

        if (fileExists("edge")) {
          proxyRootDirectory = "edge"
        }

        echo "proxyRootDirectory : ${proxyRootDirectory}"

        if (fileExists("java")) {
          javaDirectoryExists = true
        }

      }

      stage('Checkout') {
        SCMUtils scmUtils = new SCMUtils()
        giturl = scmUtils.getRepoUrl("", "", params.api)
        deployVersion = params.version
        git url: giturl, branch: 'develop', credentialsId: config.scm.credential
        def shell = new shell()
        shell.pipe("git checkout tags/${deployVersion} -b ${deployVersion}")
      }


      stage('deploy-remove-proxy') {


        if (javaDirectoryExists) {
          dir('java') {
            runCommand "mvn package"
          }
        }

        dir(proxyRootDirectory) {
          echo "Package proxy"
          withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                            credentialsId   : config.apigee.mgmt.credential,
                            usernameVariable: 'edgeUser',
                            passwordVariable: 'edgePassword']]) {
            if(config.apigee.mgmt.OAuth){
              apigeeMavenOptions = apigeeMavenGlobalOptions +
                "-Dusername=${env.edgeUser} " +
                "-Dpassword=${env.edgePassword} " +
                "-Dauthtype=oauth";
            }
            else{
              apigeeMavenOptions = apigeeMavenGlobalOptions +
                "-Dusername=${env.edgeUser} " +
                "-Dpassword=${env.edgePassword} " +
                "-Dauthtype=basic";
            }

            runCommand "mvn clean compile "

            runCommand "mvn package " +
              " -Ddeployment.suffix=cicd " +
              apigeeMavenOptions

            if ( !params.action || (params.action as String).toLowerCase().trim().equals("deploy")) {
              if (fileExists("resources/edge/env/${edgeEnvName}/caches.json")) {
                echo "upload Caches "
                runCommand "mvn " + apigeeMavenOptions +
                        " apigee-config:caches"
              }

              if (fileExists("resources/edge/env/${edgeEnvName}/targetServers.json")) {
                echo "upload Target Servers"
                runCommand "mvn " + apigeeMavenOptions +
                        " apigee-config:targetservers"
              }

              if (fileExists("resources/edge/env/${edgeEnvName}/kvms.json")) {
                echo "upload KVMs"
                runCommand "mvn  " + apigeeMavenOptions +
                        " apigee-config:keyvaluemaps "
              }

              echo "deploy the proxy bundle"
              runCommand "mvn apigee-enterprise:deploy " +
                      "-Ddeployment.suffix=cicd " +
                      apigeeMavenOptions
            }

            if ( params.action && (params.action as String).toLowerCase().trim().equals("un-deploy")) {

              echo "Undeploy the proxy bundle"
              runCommand "mvn apigee-enterprise:deploy " +
                      "-Ddeployment.suffix=cicd -Dapigee.options=clean " +
                      apigeeMavenOptions
            }
          }
        }
      }

        if (config.jenkins.emailOnSuccess) {
          currentBuild.result = 'SUCCESS'
        }
    }catch (any) {
        currentBuild.result = 'FAILURE'
      }

  }
}


def getApigeeMavenOptions(Map apigeeEnvInfo) {
  def apigeeMavenOptions = " -Papigee  " +
    "-Denv=${apigeeEnvInfo.env} " +
    "-Dorg=${apigeeEnvInfo.org} " +
    "-Dusername=${env.edgeUser} " +
    "-Dpassword=${env.edgePassword} ";
  return apigeeMavenOptions as String;
}




def getPom() { return new pom(); }

def runCommand(String command) {
  if (!isUnix()) {
    println command
    if (command.trim().toLowerCase().startsWith("mvn")) {
      withMaven(globalMavenSettingsConfig: 'demo-maven-settings', maven: 'apigee-maven') {
        bat returnStdout: true, script: "${command}"
      }
    } else {

      bat returnStdout: true, script: "${command}"
    }
  } else {
    println command
    if (command.trim().toLowerCase().startsWith("mvn")) {
      withMaven(globalMavenSettingsConfig: 'demo-maven-settings', maven: 'apigee-maven') {
        sh returnStdout: true, script: command
      }
    } else {
      sh returnStdout: true, script: command
    }

  }
}


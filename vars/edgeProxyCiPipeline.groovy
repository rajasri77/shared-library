import CICDEnvUtils
import pom

def call ( String branch_name, String build_number) {

    envUtils = new CICDEnvUtils() ;
    def config = envUtils.getEnviromentConfigs()
    String apigeeMavenGlobalOptions = envUtils.getApigeeMavenOptions(branch_name);
    def apigeeEnvInfo;

    if( config.apigee.edge.envs.dev ) {
        apigeeEnvInfo = config.apigee.edge.envs.dev
    }

    file apidir = new java.io.File( "edge" )
    file javadir = new java.io.File ( "java" )

    def scmVars ;

    boolean apiDirectoryExists ;
    boolean javaDirectoryExists ;
    def apiDirectory ;

    stage('build-setup') {
        node {
//          checkout scm
            echo "config.com.cicd.apigee.edge.envs =  ${config.apigee.edge.envs}"
            currentBuild.displayName = "${branch_name}-${build_number}"
            echo " Build Name reset ${currentBuild.displayName}"
            echo " Apigee Maven Options : ${apigeeMavenGlobalOptions}"


            // Validate that edge creds are setup
            try {
                withCredentials([[$class: 'UsernamePasswordMultiBinding',
                                  credentialsId: config.apigee.edge.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {}

            } catch (Exception e) {
                error "Missing Credentials : - "  + e.getMessage()
            }

            withCredentials([[$class: 'UsernamePasswordMultiBinding',
                              credentialsId: config.apigee.edge.credential,
                              usernameVariable: 'edgeUser',
                              passwordVariable: 'edgePassword']]) {
                apigeeMavenOptions = apigeeMavenGlobalOptions +
                        "-Dusername=${env.edgeUser} " +
                        "-Dpassword=${env.edgePassword} ";

                echo " Apigee Maven Options : ${apigeeMavenOptions}"
            }
            if ( javadir.isDirectory()) {
                javaDirectoryExists = true
            }
            if ( apidir.isDirectory()){
                apiDirectoryExists = true
                apiDirectory = "api"
            } else if ( (new File ("apiproxy")).isDirectory()) {
                apiDirectoryExists = true
                apiDirectory = "."
            }

            // Fail the job is api directory does not exist
            if ( !  apiDirectoryExists ) {
              echo  "Current Working Directory is ${pwd} "
              error "This project cannot be supported with CICD Pipeline. " +
                "Missing Required Directory Structure "
            }
        }
    }

    if( javadir.exists() ) {
        stage('build-java-callout') {
            node {
                dir('java') {
                    runCommand  "mvn clean package"
                }
            }
        }
    }

    stage('proxy-lint') {
        node {
            dir(apiDirectory) {
                script {
                    try {
                        runCommand  "apigeelint -s apiproxy -f table.js"
                    } catch (err) {
                        echo "Failed Proxy Lint Validation"
                    }
                }
            }
        }
    }

    stage('unit-test') {
        node {
            dir(apiDirectory) {
                echo "run unit tests "
                runCommand  "mvn test -Pproxy-unit-test"
                if ( false ) {
                    echo "publish html report"
                    step (
                            publishHTML(target: [
                                    allowMissing         : false,
                                    alwaysLinkToLastBuild: false,
                                    keepAll              : false,
                                    reportDir            : "target/unit-test-coverage/lcov-report",
                                    reportFiles          : 'index.html',
                                    reportName           : 'HTML Report'
                            ]
                            ))
                }
            }
        }
    }

    stage('build-proxy') {
        node {
            dir(apiDirectory) {
                echo "Package proxy"
                withCredentials([[$class       : 'UsernamePasswordMultiBinding',
                                  credentialsId: config.apigee.edge.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {
                    def apigeeMavenOptions = apigeeMavenGlobalOptions +
                            "-Dusername=${env.edgeUser} " +
                            "-Dpassword=${env.edgePassword} ";

                    if ( config.apigee.edge.noDeploy ) {
                        echo "mvn package " +
                                " -Ddeployment.suffix=${params.deployment_suffix} " +
                                apigeeMavenOptions

                    } else {
                        runCommand  "mvn package " +
                                " -Ddeployment.suffix=${params.deployment_suffix} " +
                                apigeeMavenOptions
                    }


                }
            }
        }
    }

    stage('pre-deploy-prep') {
        node {
            withCredentials([[$class: 'UsernamePasswordMultiBinding',
                              credentialsId: config.apigee.edge.credential,
                              usernameVariable: 'edgeUser',
                              passwordVariable: 'edgePassword']]) {
                def apigeeMavenOptions = apigeeMavenGlobalOptions +
                        "-Dusername=${env.edgeUser} " +
                        "-Dpassword=${env.edgePassword} ";

                dir(apiDirectory) {
                    if ( config.apigee.edge.noDeploy ) {
                    echo "upload Caches "
                    echo "mvn " + apigeeMavenOptions +
                            " com.cicd.apigee-config:caches"
                    echo "upload Target Servers"
                    echo "mvn " + apigeeMavenOptions +
                            " com.cicd.apigee-config:targetservers"
                    echo "upload KVMs"
                    echo "mvn  " + apigeeMavenOptions +
                            " com.cicd.apigee-config:keyvaluemaps "
                } else {
                        echo "upload Caches "
                        runCommand  "mvn " + apigeeMavenOptions +
                                " com.cicd.apigee-config:caches"
                        echo "upload Target Servers"
                        runCommand  "mvn " + apigeeMavenOptions +
                                " com.cicd.apigee-config:targetservers"
                        echo "upload KVMs"
                        runCommand  "mvn  " + apigeeMavenOptions +
                                " com.cicd.apigee-config:keyvaluemaps "
                    }

                }
            }
        }
    }
    stage('deploy-proxy') {
        if ( ! config.apigee.edge.noDeploy ) {

            node {
                withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                                  credentialsId   : config.apigee.edge.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {

                  def apigeeMavenOptions = apigeeMavenGlobalOptions +
                    "-Dusername=${env.edgeUser} " +
                    "-Dpassword=${env.edgePassword} ";

                    dir(apiDirectory) {
                        echo "deploy the proxy bundle"
                        runCommand  "mvn com.cicd.apigee-enterprise:deploy " +
                                "-Ddeployment.suffix=${params.deployment_suffix} " +
                                apigeeMavenOptions
                    }
                }
            }
        }
    }
    stage('integration-tests') {
        if ( ! config.apigee.edge.noDeploy ) {
            node {
            dir(apiDirectory) {
                withCredentials([[$class: 'UsernamePasswordMultiBinding',
                                  credentialsId: config.apigee.edge.credential,
                                  usernameVariable: 'edgeUser',
                                  passwordVariable: 'edgePassword']]) {
                    def apigeeMavenOptions = apigeeMavenGlobalOptions +
                            "-Dusername=${env.edgeUser} " +
                            "-Dpassword=${env.edgePassword} ";

                echo "load api product for integration test"
                runCommand  "mvn " + apigeeMavenOptions + " com.cicd.apigee-config:apiproducts"

                echo "load api developer for integration test"
                runCommand  "mvn " + apigeeMavenOptions + " com.cicd.apigee-config:developers "

                echo "load api developer app for integration test"
                runCommand  "mvn " + apigeeMavenOptions + " com.cicd.apigee-config:apps"

                echo "export app key for integration test"
                runCommand  "mvn com.cicd.apigee-config:exportAppKeys " + apigeeMavenOptions ;
                // "-Ddeployment.suffix=${params.deployment_suffix} " ;

                echo "run integration test"
//            runCommand  "mvn -Pproxy-integration-test test"
                runCommand  "node ./node_modules/cucumber/bin/cucumber.js " +
                        "target/test/integration/features " +
                        "--format json:target/reports.json"
                }

                echo "genereate integration test report "
                step([
                        $class             : 'CucumberReportPublisher',
                        fileExcludePattern : '',
                        fileIncludePattern : "**/reports.json",
                        ignoreFailedTests  : false,
                        jenkinsBasePath    : '',
                        jsonReportDirectory: "target",
                        missingFails       : false,
                        parallelTesting    : false,
                        pendingFails       : false,
                        skippedFails       : false,
                        undefinedFails     : false
                ])
            }
        }
        }
    }
  stage('upload-artifact') {
    if ( ! config.apigee.edge.noDeploy ) {
      node {
        dir(apiDirectory) {
          runCommand  "mvn deploy -DskipTests=true"
          def pomVersion = getPom().version("pom.xml")
          runCommand  "git tag ${pomVersion}"
          runCommand  "git push origin --tags"
        }
      }
    }
  }

}

def getApigeeMavenOptions(Map apigeeEnvInfo) {
    def apigeeMavenOptions = " -Papigee  " +
            "-Denv=${apigeeEnvInfo.env} " +
            "-Dorg=${apigeeEnvInfo.org} " +
            "-Dusername=${env.edgeUser} " +
            "-Dpassword=${env.edgePassword} ";
    return apigeeMavenOptions as String;
}


def getPom() {return new pom();}

def runCommand(String command) {
  if (!isUnix()) {
     bat command
  } else {
    sh command
  }
}

def isOnWindows(){
  def os = "windows"
  def List nodeLabels  = NODE_LABELS.split()
  for (i = 0; i <nodeLabels.size(); i++)
  {
    if (nodeLabels[i]==os){
      return true
    }
  }

  return false
}

@Grab(group = 'org.codehaus.groovy.modules.http-builder',
  module = 'http-builder', version = '0.7')

import groovy.transform.Field

@Field LogUtils log = new LogUtils();
@Field CICDEnvUtils envUtils = new CICDEnvUtils();
@Field HttpUtils httpUtil = new HttpUtils();

String getHttpResponse(def response) {
  def httpStatus = response.status
  if (response.status != 404 && (httpStatus >= 100 && httpStatus <= 399)) {
    return response.content as String
  } else {
    ""
  }
}




void addUserToRole(String username, String roleName, String org) {

}

boolean isUserInRole(String username, String roleName, String org) {

  return true

}


protected Map<String, String> getMgmtServerCreds() {
  return envUtils.getCredentials(envUtils.getConfig().apigee.mgmt.credential)
}

protected String getAuthToken() {
  def creds = envUtils.getCredentials(envUtils.getConfig().apigee.mgmt.credential)
  if (creds.username && creds.password) {
    authToken = getBasicAuthCreds(creds.username, creds.password)
    return authToken
  } else {
    log.fatal("Credential " + envUtils.getConfig().apigee.mgmt.credential + " is not setup correctly ")
  }
}

def getBasicAuthCreds(String u, String p) {
  return httpUtil.getBasicAuthCreds(u, p)
}

def getApigeeServerUrl() {
  return new EdgeRoles().getApigeeServerUrl(envUtils.getConfig().apigee.mgmt)
}

return this;

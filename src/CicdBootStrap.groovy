import com.cicd.CicdBase

try {
  if ( CicdBase.config == null || CicdBase.config.isEmpty() ) {
    def configFileJson = libraryResource "config/active-config"
    println "Getting config File " + configFileJson
    def envConfig = new configslurper().slurpJsonResourceToMap(configFileJson as String, [:])
    echo "DEBUG: Got envConfig:  ${envConfig}"
    new CicdBase(config: envConfig)
  }
} catch (Exception err) {
  err.printStackTrace()
  println(err.getMessage())
}

return this

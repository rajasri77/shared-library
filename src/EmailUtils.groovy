import hudson.model.Cause


def emailNotificationSuccess() {

}

def emailNotificationFailure() {

}


def boolean isBuildCauseUserAction() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    println("The Cause is "+ cause)
    if (cause instanceof hudson.model.Cause.UserIdCause) return true
  }
  return false
}

String getUserIdForBuild() {
  def causes = currentBuild.rawBuild.getCauses()
  for (Cause cause in causes) {
    def user;
    if (cause instanceof hudson.model.Cause.UserIdCause) {
      hudson.model.Cause.UserIdCause userIdCause = cause;
      user = userIdCause.getUserId()
      return user
    }
  }
  return null
}

def runCommand(String command) {
  if (!isUnix()) {
    println command
    bat returnStdout: true, script: "${command}"
  } else {
    println command
    sh returnStdout: true, script: command
  }
}

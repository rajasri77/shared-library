import groovy.transform.Field

@Field LogUtils log = new LogUtils()

String getBasicAuthCreds(String username, String password) {
    return "Basic " + "${username}:${password}".bytes.encodeBase64().toString()

}

def  doPost(String httpUrl, String requestData, String authToken) {

    def response = httpRequest acceptType: 'APPLICATION_JSON',
            httpMode: 'POST',
            customHeaders: [[name: 'Authorization', value: authToken]],
            url: httpUrl,
            requestBody: requestData

  log.debug ("Response : ${response.status}")
    return response;

}

def doGet(String httpUrl, Map queryParams, String authToken) {

    String paramString = "";
    if (queryParams != null && !queryParams.isEmpty()) {
        paramString += "?"
        for (s in queryParams) {
            paramString += java.net.URLEncoder.encode(s, "UTF-8")
            +"="
            +java.net.URLEncoder.encode(queryParams[s], "UTF-8")
        }
    }

    println(httpUrl + paramString)

    def response = httpRequest acceptType: 'APPLICATION_JSON',
            httpMode: 'GET',
            customHeaders: [[name: 'Authorization', value: authToken]],
            url: httpUrl + paramString,
            validResponseCodes : "100:404"


    log.debug ("Response : ${response.status}")

    return response;

}

def doDelete(String httpUrl, Map queryParams, String authToken) {

  String paramString = "";
  if (queryParams != null && !queryParams.isEmpty()) {
    paramString += "?"
    for (s in queryParams) {
      paramString += java.net.URLEncoder.encode(s, "UTF-8")
      +"="
      +java.net.URLEncoder.encode(queryParams[s], "UTF-8")
    }
  }

  println(httpUrl + paramString)

  def response = httpRequest acceptType: 'APPLICATION_JSON',
    httpMode: 'DELETE',
    customHeaders: [[name: 'Authorization', value: authToken]],
    url: httpUrl + paramString

  log.debug ("Response : ${response.status}")


  return response;

}

return this;

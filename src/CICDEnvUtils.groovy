import groovy.transform.Field

@Field private static Map configMap = null ;
@Field private static boolean initialized = false ;
@Field def static instance = new CICDEnvUtils()

def getTemplate() {
  return new template()
}

def getConfigSluper() {
  return new configslurper();
}

/**
 * Returns a Pipeline Shared library resource as string
 * @param resourcename
 * @return
 */
String getResourceAsString(String resource, Map<String, String> configmap) {
  def raw = libraryResource resource
//  echo "DEBUG: Got :  ${raw}"
  def tempString = getTemplate().transform(raw as String, configmap)
//  echo "DEBUG: Got :  ${tempString}"
  return tempString
}

/**
 * Get the configuration for the CICD jobs
 * @return
 */
Map getEnviromentConfigs() {
  return getConfig()
}

Map getConfig() {

  println(" Getting Configuration ")

  if ( configMap == null ) {
    println(" Initializing  Configuration ")
    init()
  } else {
    println(" Found Configuration ")
  }
  return configMap
}

/**
 * get the USERNAME  and PASSWORD
 * Map Contains {user:"", password:"}* @param credId
 * @return
 */
Map<String, String> getCredentials(String credId) {
  def cred = [:]
  if (credId == null) {
    echo "credId is null "
    return credId
  }
  withCredentials([[$class          : 'UsernamePasswordMultiBinding',
                    credentialsId   : credId,
                    usernameVariable: 'user',
                    passwordVariable: 'password']]) {
    if (env.user == null || env.password == null) {
      echo "Credential ${credId} is not configured correctly. Missing Usename/ Password. The build will fail "
      error "Credential Configuration Error : ${credId}"
      throw new Exception("Credential Configuration Error : ${credId}")
    }
    cred.username = env.user
    cred.password = env.password

//      println ("--->> "+ cred)
  }

  return cred
}

/**
 * given the name jenkins managed file, gets the location of the location of the file on jenkins
 * @return
 */
String getMavenSettingsFileId() {
  def mavenConfigs = getEnviromentConfigs().maven
  if (mavenConfigs && mavenConfigs.settingsFileId) {
    return mavenConfigs.settingsFileId as String
  }
  return null
}

/**
 * Get the Apigee Maven Configs for a given Environment
 * @param env
 * @return
 */
String getApigeeMavenOptions(String env) {
  if ( env == null || env.isEmpty() ) assert(false)
  if ( config == null ) {init()}
  def apigeeEnvs = getEnviromentConfigs().apigee.edge.envs;
  def apigeeMgmtServerCfg = getEnviromentConfigs().apigee.mgmt
  if (apigeeEnvs[env]) {
    def envCfgs = apigeeEnvs[env]
    def apigeeOptions = "-Papigee -Denv=${envCfgs.env} " +
      "-Dorg=${envCfgs.org} " +
      "-DvhostProtocol=${envCfgs.vhost.protocol} " +
      "-DvhostDomainName=${envCfgs.vhost.domain.name} " +
      "-DvhostDomainPort=${envCfgs.vhost.domain.port} " +
      "-DvhostEdgeName=${envCfgs.vhost.edgeName} " +
      "-Dapigee.config.dir=${apigeeEnvs.configDir} " +
      "-Dapigee.config.options=${apigeeEnvs.configOptions} " +
      "-Dapigee.config.exportDir=${apigeeEnvs.configExportDir} ";

    if (apigeeMgmtServerCfg.apiPort) {
      apigeeOptions += " -Dapigee.api.port=${apigeeMgmtServerCfg.apiPort} ";
    }

    if (apigeeMgmtServerCfg.apiHost) {
      apigeeOptions += " -Dapigee.api.host=${apigeeMgmtServerCfg.apiHost} ";
    }

    if (apigeeMgmtServerCfg.apiProtocol) {
      apigeeOptions += " -Dapigee.api.protocol=${apigeeMgmtServerCfg.apiProtocol} ";
    }

    if (envCfgs.tags) {
      apigeeOptions += " -Dapi.testtag=${envCfgs.tags}"
    }
    return apigeeOptions
  } else {
    echo "unable to find env [${env}]"
    return ""
  }
}

/**
 * The branch type is defined based on the config.json file used
 * @param branch_name
 * @return
 */
String get_branch_type(String branch_name) {
  //Must be specified according to <flowInitContext> configuration of jgitflow-Maven-plugin in pom.xml
  config = getEnviromentConfigs();
//    def dev_pattern = ".*develop"
//    def release_pattern = ".*release/.*"
//    def feature_pattern = ".*feature/.*"
//    def hotfix_pattern = ".*hotfix/.*"
//    def master_pattern = ".*master"
  if (branch_name =~ config.scm.branch_pattern.dev) {
    return "dev"
  } else if (branch_name =~ config.scm.branch_pattern.release) {
    return "release"
  } else if (branch_name =~ config.scm.branch_pattern.master) {
    return "master"
  } else if (branch_name =~ config.scm.branch_pattern.feature) {
    return "feature"
  } else if (branch_name =~ config.scm.branch_pattern.hotfix) {
    return "hotfix"
  } else {
    return null;
  }
}



String get_org_type(String branch_name) {
  //Must be specified according to <flowInitContext> configuration of jgitflow-Maven-plugin in pom.xml
  def dev_pattern = ".*develop"
  def release_pattern = ".*release/.*"
  def feature_pattern = ".*feature/.*"
  def hotfix_pattern = ".*hotfix/.*"
  def master_pattern = ".*master"
  if (branch_name =~ dev_pattern) {
    return "dev"
  } else if (branch_name =~ release_pattern) {
    return "release"
  } else if (branch_name =~ master_pattern) {
    return "master"
  } else if (branch_name =~ feature_pattern) {
    return "feature"
  } else if (branch_name =~ hotfix_pattern) {
    return "hotfix"
  } else {
    return null;
  }
}

/**
 * Apigee Deployment Environments
 * @param branch_type
 * @return
 */
String get_branch_deployment_environment(String branch_type) {
  if (branch_type == "dev" || branch_type == "feature") {
    return "dev"
  } else if (branch_type == "release") {
    return "integration"
  } else {
    return null;
  }
}

def mvn(String goals) {
  def mvnHome = tool "Maven-3.2.3"
  def javaHome = tool "JDK1.8.0_102"

  withEnv(["JAVA_HOME=${javaHome}", "PATH+MAVEN=${mvnHome}/bin"]) {
    sh "mvn -B ${goals}"
  }
}

def version() {
  def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
  return matcher ? matcher[0][1] : null
}

void setConfig(String configFileJson) {
  try {
    println "DEBUG: Getting config File " + configFileJson.trim()
    def envConfig = new configslurper().slurpJsonResourceToMap(configFileJson.trim() as String, [:])
    configMap = envConfig as Map

  } catch (any) {
    println(any)
    error any.getLocalizedMessage()
  }
}


 void init() {
  try {
    def configFileJson = libraryResource "config/active-config"
    setConfig(configFileJson)
  } catch (Exception err) {
    err.printStackTrace()
    println(err.getMessage())
  }
}



return this;

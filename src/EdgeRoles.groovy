

import com.cicd.apigee.CustomRole
import groovy.json.JsonOutput

def getenvutils() {
  return new CICDEnvUtils();
}

private getApigeeServerUrl (Map mgmtInfo) {
  String mgmtUrl = "${mgmtInfo.apiProtocol}://${mgmtInfo.apiHost}:${mgmtInfo.apiPort}/${mgmtInfo.version}"
  return  mgmtUrl
}

return this

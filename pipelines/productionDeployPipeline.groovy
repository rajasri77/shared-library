#!groovy

/**
 * Jenkinsfile to be used for Production Deployment API Pipelines with Java Call Out Archetype
 */


properties([[$class  : 'BuildDiscarderProperty',
             strategy: [$class: 'LogRotator', numToKeepStr: '10']]])


if ( !params.environment ) error ("Environment Name is Required")

productionDeliveryPipeline params.environment , env.BUILD_NUMBER


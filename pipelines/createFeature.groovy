#!groovy


node {

    CICDEnvUtils utils = new CICDEnvUtils()
    SCMUtils scmUtils = new SCMUtils()


    stage("init") {
        deleteDir()
    }

    if ((params.operation as String).equals("feature-create")) {
        stage("feature-create") {
            new BranchManagerService().createFeature(params.feature, params.team, params.project, params.api)
        }
    }

    if ((params.operation as String).equals("feature-close")) {
        stage("feature-close") {
            new BranchManagerService().finishFeature(params.feature, params.team, params.project, params.api)
        }
    }

}

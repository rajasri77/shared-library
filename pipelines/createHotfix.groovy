#!groovy

node {

    CICDEnvUtils utils = new CICDEnvUtils()
    SCMUtils scmUtils = new SCMUtils()


    stage("init") {
        deleteDir()
    }



    if ((params.operation as String).equals("hotfix-start")) {
        stage("hotfix-start") {
            new BranchManagerService().createHotFix(params.team, params.project, params.api)
        }
    }

    if ((params.operation as String).equals("hotfix-close")) {
        stage("hotfix-close") {
            new BranchManagerService().finishHotFix(params.team, params.project, params.api)
        }
    }
}





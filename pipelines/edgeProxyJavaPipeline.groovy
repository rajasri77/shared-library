#!groovy


validateCicdSetup("dev")

node {

  CICDEnvUtils utils = new CICDEnvUtils()


  stage ("init") {

    println("Clear the workspace")
    deleteDir()

  }

  stage("validate-input") {

    if ( ! params.team ) {
      error "Team Name is Required"
    }

    if ( ! params.apiName )
      error "API Name is required"
  }

  stage("generate-api") {
    CICDEnvUtils envUtils = new CICDEnvUtils()
    String settingsFile = envUtils.getConfig().maven.settingsFileId
    mavenRunner = new Maven();
    println "using Maven Settings File with Name : ${settingsFile}"
    withMaven(globalMavenSettingsConfig: settingsFile) {
      def command ="-DarchetypeGroupId=com.markel.archetype " +
        " -DgroupId=ignore.this " +
        " -Dpackage=ignorethis " +
        " -DarchetypeArtifactId=java-callout-archetype " +
        " -DarchetypeVersion=${params.version} " +
        " -DartifactId=${params.apiName} " +
        " -DteamName=${params.team} " +
        " -DinteractiveMode=false " +
        " -U"

      if ( isUnix() )
        sh "mvn ${command} archetype:generate"
      else bat "mvn ${command} archetype:generate"
    }

    def downloadUrl = "${env.BUILD_URL}/execution/node/20/ws"
    echo "API Generated Can be Found Here : ${downloadUrl}"
  }

}


#!groovy


node {

  CICDEnvUtils utils = new CICDEnvUtils()


  if ((params.operation as String).equals("feature-create")) {
    stage("feature-create") {
      new BranchManagerService().createFeature(params.feature, params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-candidate-create")) {
    stage("release-candidate") {
      new BranchManagerService().createReleaseCandidate(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("feature-close")) {
    stage("feature-close") {
      new BranchManagerService().finishFeature(params.feature, params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-start")) {
    stage("release-start") {
      new BranchManagerService().createRelease(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("release-close")) {
    stage("release-close") {
      new BranchManagerService().finishRelease(params.team, params.project, params.api)
    }
  }


  if ((params.operation as String).equals("hotfix-start")) {
    stage("hotfix-start") {
      new BranchManagerService().createHotFix(params.team, params.project, params.api)
    }
  }

  if ((params.operation as String).equals("hotfix-close")) {
    stage("hotfix-close") {
      new BranchManagerService().finishHotFix(params.team, params.project, params.api)
    }
  }
}

